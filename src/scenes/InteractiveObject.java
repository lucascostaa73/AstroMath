package scenes;

import org.newdawn.slick.Graphics;

public interface InteractiveObject {
	public Entity getEntity();
	public void touchHappend();
	public void draw(Graphics g);
	public void update();
	public Boolean getVisibility();
}
