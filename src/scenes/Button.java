package scenes;

import org.newdawn.slick.Graphics;

public class Button extends Entity implements InteractiveObject {
	
	public int state = 0;
	public int type = 0;
	public ButtonDelegate delegate = null;
	public boolean enabled = true;
	public boolean visible = true;
	
	@Override
	public Entity getEntity() {
		return this;
	}
	
	public void update(){
		super.update();
	}
	
	public void draw(Graphics g){
		
		if (visible == false){
			return;
		}
		
		super.draw(g);
	}

	@Override
	public void touchHappend() {
		if (enabled && visible){
			if (delegate != null) {
				delegate.didTouched(this);
			}else{
				System.out.println("touched!");
			}
		}else{
			//Button not enabled
		}
	}
	
	@Override
	public Boolean getVisibility(){
		return this.visible;
	}

}
