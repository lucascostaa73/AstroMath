package scenes;

import java.util.ArrayList;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.MouseListener;

public class HudManager implements MouseListener, KeyListener {

	private ArrayList<InteractiveObject> objs;
	private ArrayList<InteractiveObject> aux;

	public HudManager() {
		this.objs = new ArrayList<InteractiveObject>();
		this.aux = new ArrayList<InteractiveObject>();
	}

	public void renderObjs(Graphics g) {
		for (int i = 0; i < objs.size(); i++){
			objs.get(i).draw(g);
		}
	}
	
	public void update() {
		for (int i = 0; i < objs.size(); i++){
			objs.get(i).update();
		}
	}

	public void addObj(InteractiveObject obj) {
		if (objs.contains(obj) == false) {
			objs.add(obj);
		}
	}

	public void removeObj(InteractiveObject obj) {
		if (objs.contains(obj) == true) {
			objs.remove(obj);
		}
	}

	public void removeAllObjs() {
		objs.clear();
	}

	@Override
	public void inputEnded() {
		// TODO Auto-generated method stub
	}

	@Override
	public void inputStarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isAcceptingInput() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void setInput(Input arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseClicked(int button, int x, int y, int arg3) {
		if (button == 0) {
			
//			System.out.println(x);
//			System.out.println(y);
//			System.out.println("Mouse click!");

			for (InteractiveObject obj : objs) {
				
				Entity e = obj.getEntity();
				
				float px = e.x;
				float py = e.y;
				float w = e.width * e.scale;
				float h = e.height * e.scale;
				
				if (x > px && x < px + w) {
					if (y > py && y < py + h) {
						aux.add(obj); // touch happend
					} 
				}
			}
			
			for ( InteractiveObject obj: aux){
				if (obj.getVisibility()){
					obj.touchHappend();
				}
			}
			
			aux.clear();
		}
	}

	@Override
	public void mouseDragged(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		System.out.println("Mouse Pressed");
	}

	@Override
	public void mouseReleased(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		System.out.println("Mouse Released");
	}

	@Override
	public void mouseWheelMoved(int arg0) {
		// TODO Auto-generated method stub

	}

	// Keyboard
	@Override
	public void keyPressed(int arg0, char arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(int arg0, char arg1) {
		// TODO Auto-generated method stub

	}

}
