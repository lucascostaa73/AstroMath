package scenes;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.SlickException;

public class MenuScene extends Scene implements ButtonDelegate {

	private Image background;
	private HudManager hud = null;
	
	@Override
	public void initialSetup() {
		System.out.println("Menu Scene");
		
		hud = new HudManager();
	}

	@Override
	public void configureUI() throws SlickException {
		
		background = new Image("src/assets/menu_2.png");
		
		Button start = new Button();
		start.x = 0;
		start.y = 0;
		start.width = game.window_width;
		start.height = game.window_heigth;
		start.delegate = this;
		
		hud.addObj(start);
	}

	@Override
	public void finalSetup() {}

	@Override
	public void update() {
	}
	
	@Override
	public void render(Graphics g) {
		//Draw elements
		background.draw();
		hud.renderObjs(g);
	}

	@Override
	public MouseListener getMouseListener(){ 
		return hud; 
	}
	
	public void startGame(){
		GameScene scene = new GameScene();
		game.transitTo(scene);
	}

	@Override
	public void didTouched(Button bt) {
		System.out.println("Starting game!");
		startGame();
	}

	@Override
	public void secret() {
		// TODO Auto-generated method stub
		
	}	
}
