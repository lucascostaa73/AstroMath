package scenes;

import org.newdawn.slick.SlickException;

import game.GameController;
import momentController.GameMomentController;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.MouseListener;

public class GameScene extends Scene implements ButtonDelegate, KeyListener {

	private HudManager hud;
	
	private Player player;
	private Image background;
	private Image next_background;
	private GameMomentController momentController;
	private boolean inputEnabled = false;
	private boolean animationDidEnd = false;
	
	private int posy = 0;
	
	@Override
	public void initialSetup() {
		hud = new HudManager();
		System.out.println("Game Scene");
		player = new Player();
		
		momentController = new GameMomentController();
		momentController.windowHeight = game.window_heigth;
		momentController.windowWidth = game.window_width;
		momentController.hud = this.hud;
	}

	@Override
	public void configureUI() throws SlickException {
		player.setImage("ship.png");
		player.width = GameController.singleton.window_width/17;
		player.height =  player.width*1.5f;
		player.x = game.window_width/2 - player.width/2;
		player.y = game.window_heigth;
		
		background = new Image("assets/background_game.png");
		next_background = background.copy();
	}

	@Override
	public void finalSetup() {
	}

	@Override
	public void update() {
		posy += 6;
		if (posy > game.window_heigth){
			posy = 0;
		}
		
		player.update();
		momentController.update();
		
		if (!animationDidEnd){
			animationHandler();
		}
		
		hud.update();
	}
	
	@Override
	public void render(Graphics g) {
		background.draw(0, posy, this.game.window_width, this.game.window_heigth);
		next_background.draw(0, posy - game.window_heigth, this.game.window_width, this.game.window_heigth);
		player.draw(g);
		momentController.render(g);
		hud.renderObjs(g);
	}

	@Override
	public void didTouched(Button bt) {
		// TODO Auto-generated method stub
		
	}
	
	public MouseListener getMouseListener(){ 
		return hud; 
	}
	public KeyListener getKeyListener(){ 
		return this; 
	}

	@Override
	public void inputEnded() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputStarted() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isAcceptingInput() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void setInput(Input arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(int arg0, char arg1) {
		
		if (!inputEnabled) return;
		
		int vel = 10;
		
		// arrows
		switch(arg0){
			case 200: // up
				player.velocityY = -vel;
				break;
			case 208: // down
				player.velocityY = vel;
				break;
			case 203: // left
				player.velocityX = -vel;
				break;
			case 205: // right
				player.velocityX = vel;
				break;
		}
		
	}

	@Override
	public void keyReleased(int arg0, char arg1) {
		
	}
	
	public void animationHandler(){
		if (player.y < game.window_heigth - GameController.singleton.window_heigth*0.2){
			player.velocityY = -1f;
			animationDidEnd = true;
			momentController.start();
		}else{
			player.velocityY = -1f;
		}
	}

	@Override
	public void secret() {
		this.momentController.secret();
	}
	
}
