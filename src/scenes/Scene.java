package scenes;

import game.GameController;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.MouseListener;
import org.newdawn.slick.SlickException;

public abstract class Scene {
	
	protected GameController game;
	
	public void initializeScene(GameController newGame) throws SlickException{
		
		this.game = newGame; 
		
		this.initialSetup();
		this.configureUI();
		this.finalSetup();
	}
	
	public abstract void initialSetup();
	public abstract void configureUI() throws SlickException;
	public abstract void finalSetup();
	
	public abstract void update();
	public abstract void render(Graphics g);
	
	public MouseListener getMouseListener(){ return null; }
	public KeyListener getKeyListener(){ return null; }
	
	public void destroy(){
		game = null;
	}
	
	public abstract void secret();
}
