package scenes;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import entity.Animation;

public class Entity {
	
	public float x;
	public float y;
	public float width;
	public float height;
	public float scale = 1;
	
	public float velocityX = 0;
	public float velocityY = 0;
	
	public Boolean visible = true;
	
	public Color color = Color.white;
	
	protected Image image = null;
	protected String imageFile = null;
	
	public Animation animation = null;
	
	
	public void setImage(String file){
		this.imageFile = file;
	}
	
	public void setImage(Image image){
		this.image = image;
	}
	
	private void setupImage(){
		try {
			this.image = new Image("assets/" + imageFile);
			this.image.setFilter(Image.FILTER_LINEAR);
		} catch (SlickException e) {
			System.out.println("Could not load image");
		}
	}
	
	public void draw(Graphics g){
		
		if (!visible){ return; }
		
		if (imageFile != null){
			setupImage();
			imageFile = null;
		}
		
		if (image != null){
			image.draw(x, y, width*scale, height*scale, color);
		}
	}
	
	public float centerX(){
		return this.x + this.width/2;
	}
	
	public float centerY(){
		return this.y + this.height/2;
	}
	
	public void update(){
		this.x += velocityX;
		this.y += velocityY;
		
		if(animation != null){
			animation.update();
		}
	}
	
	public void rotate(float angle){
		if (image != null){
			image.rotate(angle);
		}
	}
	
	public void rotate(double angle){
		if (image != null){
			image.rotate((float)angle);
		}
	}
	
	public Image getImage(){
		return this.image;
	}
}
