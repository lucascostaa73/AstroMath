package main;

import game.*;
import org.newdawn.slick.SlickException;

public class Main{
	
	public static void main(String args[]) throws SlickException {
		
		System.out.println("It's running!");
		
		GameController game = new GameController("AstroMath");
		
		
		AstroGameContainer app = new AstroGameContainer(game);
		app.setTargetFrameRate(60);
		app.setShowFPS(false);
		app.setAlwaysRender(true);
		app.setVSync(true);
		
		game.exitDelegate = app;
		
//		game.window_width = 711*2;
//		game.window_heigth = 400*2;
		
		game.window_width = 1920;
		game.window_heigth = 1080;
		
		app.setDisplayMode(game.window_width, game.window_heigth, true);
		app.start();
	}
	
}
