package entity;

import java.util.ArrayList;
import game.GameController;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import scenes.Entity;

public class RankingBoard extends Entity {

	private ArrayList<ScoreRank> scores = GameController.singleton.scores;
	public int maxScores = 10;
	public boolean visible = true;
	public ScoreRank currentBestScore;
	private int blank = 10;
	private int maxBlank = 10;
	
	public ScoreRank checkBestScore(int score){
		
		ScoreRank rank = null;
		
		if (scores.size() == 0){
			rank = new ScoreRank("", score);
			scores.add(rank);
			this.currentBestScore = rank;
			return rank;
		}
		
		for(int i = 0; i < scores.size(); i++){
			ScoreRank sc = scores.get(i);
			
			if(sc.score < score){
				rank = new ScoreRank("", score);
				scores.add(i, rank);
				break;
			}
		}
		
		if(rank == null && scores.size() < maxScores){
			rank = new ScoreRank("", score);
			scores.add(rank);		
		}
		
		this.currentBestScore = rank;
		
		return rank;
	}
	
	
	public void draw(Graphics g){
		
		if (visible == false) { 
			return; 
		}
		
		super.draw(g);
		float scale = 2;
		
		for(int i = 0; i < scores.size(); i++){
			
			ScoreRank sc = scores.get(i);
			String scoreString = String.valueOf(sc.score);
			int diff = 20 - scoreString.length();
			
			Color c = Color.black;
			String auxName = sc.name;
			
			if(sc == this.currentBestScore && this.currentBestScore != null){
				
				c = Color.blue;
				
				if(blank > 0){
					blank += 1;
					auxName = "   ";
					if (blank > maxBlank){
						blank = 0;
					}
				}else{
					blank -= 1;
					if(blank < -maxBlank*10){
						blank = 1;
					}
				}
				
				
			}
			
			g.scale(scale, scale);
			g.setColor(c);
			g.drawString(auxName + spaces(diff) + sc.score, (this.x + 50)/scale, (this.y + 50 + i*40)/scale );
			g.scale(1/scale, 1/scale);
		}
		
	}
	
	public void clear(){
		currentBestScore = null;
	}
	
	public String spaces(int i){
		
		String str = "";
		
		for(int k = 0; k < i; k++){
			str += ".";
		}
		
		return str;
	}
	
}
