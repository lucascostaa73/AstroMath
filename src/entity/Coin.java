package entity;

import java.util.Hashtable;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import scenes.Entity;


public class Coin extends Entity{
	
	public int quantity = 0;
	private Numbers numbers = new Numbers();

	public Coin(){
		numbers.height = 50;
		numbers.width = 50;
	}
	
	public class Numbers {
		
		public float x = 0;
		public float y = 0;
		public float width = 0;
		public float height = 0;
		
		public int value = 0;
		private Hashtable<Integer, Image> images = new Hashtable<Integer, Image>();
		
		
		public Numbers(){
			try {
				for(int i = 0; i < 10; i++){
					Image img = new Image("assets/numbers/" + i + ".png");
					img.setFilter(Image.FILTER_LINEAR);
							
					images.put(i, img);
				}
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		
		public void draw(){
			
			String numbers = String.valueOf(this.value);
			
			for(int i = 0; i < numbers.length(); i++){
				char c = numbers.charAt(i);
				int n = c - 48;
				//System.out.println("char: " + (int) n);
				images.get(n).draw(x + width*i, y, width, height);
			}
			
		}
	}
	
	public void update(){
		super.update();
		numbers.value = this.quantity;
		
		numbers.width = this.width/2;
		numbers.height = this.height/2;
		
		numbers.x = this.x + this.width + 2;
		numbers.y = this.y + this.height - numbers.height;
		
	}

	public void draw(Graphics g){
		super.draw(g);
		
		//Draw coin animation
//		g.setColor(Color.yellow);
//		g.drawRect(x, y, width, height);
		
		//Draw score
		numbers.draw();
			
	}
	
}
