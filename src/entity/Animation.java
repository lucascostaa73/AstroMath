package entity;

import java.util.ArrayList;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import scenes.Entity;
//import org.newdawn.slick.SlickException;

public class Animation {
	
	private ArrayList<Image> images = new ArrayList<Image>();
	private int imageIndex = 0;
	
	private Entity entity = null;
	public int frameDelay = 1;
	private int frameDelayAux = 0;
	private Image lastImageEntity = null; 
	
	public AnimationDelegate delegate = null;

	public void setImages(ArrayList<Image> images){
		this.images = images;
	}
	
	public void setImages(Image... images){
		for(Image im: images){
			this.images.add(im);
		}
	}
	
	public void setImages(String baseName, int length){

		try {
			for(int i = 1; i <= length; i++){
				Image im;
				im = new Image(baseName + String.valueOf(i) + ".png");
				if (im != null){
					im.setFilter(Image.FILTER_LINEAR);
					this.images.add(im);
				}
			}
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	
	public void run(Entity ent){
		this.entity = ent;
		this.lastImageEntity = entity.getImage();
		this.entity.animation = this;
		frameDelayAux = frameDelay;
	}
	
	public void update(){
		if (entity != null){
			if (updateAnimation()){
				Image nextImage = nextImage();
				if (nextImage != null){
					entity.setImage(nextImage);
				}else{
					this.endAnimation();
				}
			}
		}
	}
	
	private boolean updateAnimation(){
		if (frameDelayAux == 0){
			frameDelayAux = frameDelay;
			return true;
		}else{
			frameDelayAux -= 1;
		}
		
		return false;
	}
	
	private Image nextImage(){
		
		//System.out.println("update image...");
		
		if (imageIndex >= images.size()){
			return null;
		}
		
		Image img = images.get(imageIndex); 
		imageIndex += 1;
		
		return img;
	}
	
	public void endAnimation(){
		
		//System.out.println("Ending animation...");
		
		this.entity.setImage(this.lastImageEntity);
		
		if (delegate != null){
			delegate.animationDidEnd(this.entity, this);
		}
		
		this.entity.animation = null;
		this.entity = null;
		imageIndex = 0;
		this.delegate = null;
	}
	
	public Animation copy(){
		
		Animation cp = new Animation();
		cp.setImages(this.images);
		cp.frameDelay = this.frameDelay;
		
		return cp;
	}
}
