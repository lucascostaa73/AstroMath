package entity;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import scenes.Button;



public class AsteroidMath extends Button{

	public String math = "";
	public boolean clicked = false;
	
	public void draw(Graphics g){
		
		if (!visible){
			return;
		}
		
		super.draw(g);
		
		float s = 1.8f;
		float px = (this.x + (this.width*this.scale)/2)/s;
		float py = (this.y + (this.height*this.scale)/2)/s;
		
		g.scale(s,s);
		g.setColor(Color.white);
		g.drawString(math, px - math.length()*scale*5,  py);
		g.scale(1/s,1/s);
	}
	
	public void correct(){
		
	}
	
	public void wrong(){
		
	}
	
	
	
	
}
