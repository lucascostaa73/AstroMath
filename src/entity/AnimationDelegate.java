package entity;

import entity.Animation;
import scenes.Entity;

public interface AnimationDelegate {
	public void animationDidEnd(Entity entity, Animation animation);
}
