package entity.timeBar;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import scenes.Entity;

public class TimeBar extends Entity{

	public TimeBarDelegate delegate = null;
	private float counter = 0;
	public float maxCounter = 10000;
	public boolean didEnded = false;
	
	
	
	public void start(){
		didEnded = false;
		counter = maxCounter;
	}
	
	@Override
	public void draw(Graphics g){
		
		if (!visible) { return; }
		
		super.draw(g);
		
		float line = 2;
		g.setColor(Color.black);
		g.fillRect(this.x - line, this.y - line, this.width + line*2, this.height + line*2);

		g.setColor(Color.white);
		g.fillRect(this.x, this.y, this.width, this.height);

		if (counter > 0){
			
			Color some = new Color(200, 200, 255);
			g.setColor(some);
			float w = this.width*(counter/maxCounter);
			g.fillRect(this.x, this.y, w, this.height);
		}
		
	}
	
	public void finish(){
		didEnded = true;
		counter = 0;
	}
	
	@Override
	public void update(){
		if (didEnded){
			return;
		}
		
		super.update();
		
		// Counter
		if (counter <= 0){
			
			finish();
			if (delegate != null){
				delegate.timerDidEnd();
			}
			
		}else{
			counter -= 1;
		}
	}
	
}
