package entity.life;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import entity.Animation;
import entity.AnimationDelegate;
import scenes.Entity;

public class PlayerLife extends Entity{

	private ArrayList<Life> lifes = new ArrayList<Life>();
	public LifeEndedDelegate delegate; 
	private int lifeCounter = 0;
	private int lifeMax = 4;
	public float inBetween = 20;
	
	
	
	public PlayerLife(){
		
		lifeCounter = lifeMax - 1;
		
		for (int i = 0; i < lifeMax; i++){
			Life life = new Life();
			life.setImage("ship.png");
			lifes.add(life);
		}
	}
	
	public int getLife(){
		return this.lifeCounter;
	}
	
	public void loseLife(){
		if (lifeCounter >= 0 ){
			
			lifeCounter -= 1;
			
			System.out.println(lifeCounter);
			Life life = lifes.get(lifeCounter);
			life.loseAnimation();
			
			
		}
	}
	
	public void gainLife(){
		
		if (lifeCounter < lifeMax - 1){
			Life life = lifes.get(lifeCounter);
			life.gainAnimation();
			lifeCounter += 1;
		}
		
		//If life is full -> lifeFull animation		
	}
	
	public void update(){
		
		for (int i = 0; i < this.lifeMax - 1; i++){
			Life life = lifes.get(i);
			
			life.width = (this.height*0.65f);
			life.height = this.height;
			
			life.x = this.x - i*life.width - i*this.inBetween;
			life.y = this.y;
			
			life.update();
		}
	}
	
	public void draw(Graphics g){
		for (int i = 0; i < this.lifeMax - 1; i++){
			Life life = lifes.get(i);
			life.draw(g);
		}
	}
	
	public void shyniAnimation(){}
	
	public void fullLifeAnimation(){
		shyniAnimation();
		jumpAnimation();
	}
	
	public void jumpAnimation(){}
	
	
	
	public class Life extends Entity implements AnimationDelegate {
		
		private Animation puff = new Animation(); 
		private boolean erased = false;
		
		public Life(){
			puff.frameDelay = 3;
			puff.setImages("assets/clouds/cloud_", 6);
			puff.delegate = this;
		}
		
		public void draw(Graphics g){
			super.draw(g);
//			g.setColor(this.color);
//			g.drawRect(this.x, this.y, height/2, height);
		}
		
		public void loseAnimation(){
			erased = true;
			puff.delegate = this;
			puff.run(this);
			this.color = Color.red;
		}
		
		@Override
		public void animationDidEnd(Entity entity, Animation animation) {
			
			System.out.println("Life ending animation...");
			
			if (erased == false) {
				this.color = Color.white;
			}else{
				this.color = Color.transparent;
			}
		}
		
		public void gainAnimation(){
			erased = false;
			System.out.println("Gaining life animation");
			puff.delegate = this;
			puff.run(this);
			this.color = Color.green;
		}
		
		public void shyniAnimation(){}
		
		public void jumpAnimation(){}
	}
	
	public int getMax(){
		return this.lifeMax;
	}
	
}
