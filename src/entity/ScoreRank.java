package entity;

public class ScoreRank {
	public String name = "";
	public int score = 0;
	
	public ScoreRank(String n, int s){
		this.name = n;
		this.score = s;
	}
}
