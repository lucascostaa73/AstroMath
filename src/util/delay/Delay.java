package util.delay;

import java.util.Timer;
import java.util.TimerTask;


public class Delay extends Timer implements DelayDelegate{

	private DelayDelegate delegate;
	private int type = 0;
	
	public Delay(DelayDelegate delegate, int type){
		this.type = type;
		this.delegate = delegate;
	}
	
	public void schedule(long delay){
		Task task = new Task(this);
		this.schedule(task, delay);
	}

	@Override
	public void delayEnded(Delay timer) {
		delegate.delayEnded(this);
	}
	
	public int getType(){
		return type;
	}
	
	
	private class Task extends TimerTask{
		
		private DelayDelegate delegate;
		
		public Task(DelayDelegate delegate){
			this.delegate = delegate;
		}
		
		public void run(){
			delegate.delayEnded(null);
		}
	}

	
}
