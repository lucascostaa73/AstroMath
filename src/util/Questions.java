package util;

import java.util.ArrayList;
import java.util.Random;

public class Questions {

	private ArrayList<QuestionAnswerPack> questions1 = new ArrayList<QuestionAnswerPack>();
	private ArrayList<QuestionAnswerPack> questions2 = new ArrayList<QuestionAnswerPack>();
	private ArrayList<QuestionAnswerPack> questions3 = new ArrayList<QuestionAnswerPack>();
	
	private ArrayList<QuestionAnswerPack> aux = new ArrayList<QuestionAnswerPack>();
	
	private QuestionAnswerPack current = null;
	Random rand = new Random(System.currentTimeMillis());
	
	
	public Questions(){
		
		//3 ano
		this.addQuestion(2, "Ao chegar no espaço, o astronauta Miguel, identificou 8 planetas e o dobro dessa quantidade de meteoros. Quantos meteoros ele conseguiu identificar no espaço?", 
				new Answer("4", 0),
				new Answer("6", 0),
				new Answer("16", 1),
				new Answer("18", 0));
		
		this.addQuestion(2, "Quando o astronauta estava de folga, começou a observar as estrelas. Conseguiu contar 100 estrelas em um dia. Quantas estrelas ele contaria se contasse por 10 dias?", 
				new Answer("20", 0),
				new Answer("50", 0),
				new Answer("1000", 1),
				new Answer("100", 0));
		
		this.addQuestion(2, "A comida da nave dos astronautas já estava acabando. Tinha apenas 25 latinhas de comida para dividir entre os 5 astronautas da nave. Quantas latinhas dariam para cada um?", 
				new Answer("3", 0),
				new Answer("5", 1),
				new Answer("15", 0),
				new Answer("7", 0));
		
		this.addQuestion(2, "O astronauta Miguel saiu da nave para resolver um problema. Ele precisava de 35 metros de corda para amarrar uma peça solta. Ao começar a fazer o serviço, notou que só tinha 15 metros de corda. Quantos metros de corda faltavam para Miguel conseguir amarrar a peça?", 
				new Answer("15", 0),
				new Answer("5", 0),
				new Answer("25", 0),
				new Answer("20", 1));
		
		this.addQuestion(2, "Miguel imaginou que levaria cerca de 5 anos para sua nave chegar a Marte. Porém, a nave demorou o triplo de anos para chegar ao seu destino. Em quantos anos Miguel chegou a Marte?", 
				new Answer("15", 1),
				new Answer("3", 0),
				new Answer("9", 0),
				new Answer("5", 0));
		
		this.addQuestion(2, "Beto imaginou que levaria cerca de 4 anos para sua nave chegar a Venus. Porém, a nave demorou o triplo de anos para chegar ao seu destino. Em quantos anos Beto chegou a Venus?", 
				new Answer("9", 0),
				new Answer("7", 0),
				new Answer("8", 0),
				new Answer("12", 1));
		
		//4 ano
		this.addQuestion(4, "Para uma missão no Espaço uma nave espacial transporta 13 astronautas. Quantos astronautas 8 naves conseguem transportar?", 
				new Answer("104", 1),
				new Answer("81", 0),
				new Answer("112", 0),
				new Answer("73", 0));
		
		this.addQuestion(4, "Um astronauta demora 5 minutos para ir e voltar do banheiro em gravidade zero. Se em um dia o astronauta vai 3 vezes ao banheiro, quantas tempo ele gasta indo ao banheiro em uma semana?", 
				new Answer("125", 0),
				new Answer("105", 1),
				new Answer("75", 0),
				new Answer("85", 0));
		
		this.addQuestion(4, "O planeta Saturno possui 25 satélites com mais de 10 quilômetros de diâmetro. Se essa quantidade de satélite fosse o triplo, quantos satélites saturno teria? ", 
				new Answer("35", 0),
				new Answer("75", 1),
				new Answer("50", 0),
				new Answer("30", 0));
		
		this.addQuestion(4, "Niel Armstrong pisou na lua em 1969. E a marca do solado de sua bota de astronauta permanece lá até os dias de hoje, pois na lua não tem vento. Sabendo que estamos em 2017,  quantos anos tem a marca da bota do astronauta?", 
				new Answer("44", 0),
				new Answer("38", 0),
				new Answer("68", 0),
				new Answer("48", 1));
		
		// 5 ano
		this.addQuestion(6, "Num momento critico da viagem, percebe-se que nao há comida para todo mundo comer. Sendo 360 sacos de comida disponíveis, para 4 tripulantes, em uma jornada de 45 dias, quantos sacos cada um pode comer por dia?", 
				new Answer("2", 1),
				new Answer("1", 0),
				new Answer("3", 0),
				new Answer("4", 0));
		
		this.addQuestion(6, "Se Urano possui uma quantidade de massa 15 vezes maior que a da Terra, e Saturno possui 90 vezes a quantidade de massa da Terra. Quantas vezes Saturno é mais pesado que Urano?", 
				new Answer("7", 0),
				new Answer("13", 0),
				new Answer("15", 0),
				new Answer("6", 1));
		
		this.addQuestion(6, "Mercúrio é o planeta mais próximo do sol. Sua temperatura pode chegar a 430°  C durante o dia e 170° C negativo durante a noite. Qual é a diferença de temperatura do dia para a noite ?", 
				new Answer("260", 1),
				new Answer("340", 0),
				new Answer("230", 0),
				new Answer("290", 0));
		
		this.addQuestion(6, "Em 20 de Agosto de 1977, a sonda Voyager2 foi lançada ao espaço levando uma mensagem de paz e amizade. Ela só retornará para a terra em 2020. Quantos anos a sonda Voyager ficará no espaço?", 
				new Answer("55", 0),
				new Answer("47", 0),
				new Answer("43", 1),
				new Answer("33", 0));
	}
	
	
	public void addQuestion(int dificult , String question, Answer... answers){
		QuestionAnswerPack q = new QuestionAnswerPack();
		
		q.dificult = dificult;
		q.question = question;
		
		for (Answer a: answers){
			q.answers.add(a);
		}
		
		if(dificult <= 2){
			questions1.add(q);
			
		} else if(dificult <= 4){
			questions2.add(q);
			
		} else if(dificult <= 6){
			questions3.add(q);
		}
	}
	
	public QuestionAnswerPack next(int dificult){
		
		QuestionAnswerPack q = this.any(questions3);
		
		if(dificult <= 1){
			System.out.println("Question lv 1");
			q = this.any(questions1);
		} else if(dificult <= 4){
			
			System.out.println("Question lv 2");
			q = this.any(questions2);
		} else if(dificult <= 6){
			
			System.out.println("Question lv 3");
			q = this.any(questions3);
		}
		
		return q;
	}
	
	
	private QuestionAnswerPack any(ArrayList<QuestionAnswerPack> questions){
		
		if (questions.isEmpty()){
			return aux.get(rand.nextInt(aux.size()));
		}
		
		int index = rand.nextInt(questions.size());  //(int) (Math.random()*questions.size());
		
		QuestionAnswerPack q = questions.get(index);
		questions.remove(q);
		
		aux.add(q);
		current = q;
		
		return q;
	}
	
	public QuestionAnswerPack current(){
		return current;
	}
	
}
