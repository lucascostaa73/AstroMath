package util;

import util.Numb;

public class AnimationMoviment {

	public double iteration = 0;
	
	public static double linearX(double x){
		return Math.cos(x);
	}
	
	public static double linearY(double x){
		return Math.sin(x);
	}
	
	public static double easeIn(Numb t, double d, double c, double b){
		
		double oldT = t.value;
		t.value /= d;
		
		return c*(t.value)*oldT + b;
			
	}
	
	public static double easeIn(Numb t, double d){
		return AnimationMoviment.easeIn(t, d, 10, 1);
	}
	
	
	// t = iteration or time
	// b = initial pos
	// c = amount of change (end - start)
	// d = total animation steps
	public static double easeInOutQuad(double t, double b, double c, double d){
		
		double mov = 0;

		if ((t/=d/2) < 1) 
			mov = c/2*t*t + b;

		mov = -c/2 * ((--t)*(t-2) - 1) + b;

		if (mov < 0 && mov < -c){
			mov = 0;
		}
		
		return mov;
	}
	
	public static double easeInOut(double t, double distance, double duration){
		return AnimationMoviment.easeInOutQuad(t, 0, distance, duration*60);
	}
	
	
	
	// Public
	public double easyInOutRepeatUpdate(double duration, double distance){
		
		this.iteration += 1;
		double fps = 60;
		
		return this.easeInOutQuadRepeat(Math.sin(iteration/10)*10, 0, distance, duration*fps);
	}
	
	private double easeInOutQuadRepeat(double t, double b, double c, double d){
		
		double mov = 0;

		if ((t/=d/2) < 1)
			mov = c/2*t*t + b;

		mov = -c/2 * ((--t)*(t-2) - 1) + b;
		
		return mov;
	}
	
}
