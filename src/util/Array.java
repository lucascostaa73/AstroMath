package util;

import java.util.ArrayList;
import java.util.Random;

public class Array<Type>{
	
	private ArrayList<Type> array;
	private Random rand = new Random(System.currentTimeMillis());
	
	@SuppressWarnings("unchecked")
	public Array(Type... objs){
		
		array = new ArrayList<Type>();
		
		for (Type obj: objs){
			array.add(obj);
		}
	}
	
	public void add(Type obj){
		array.add(obj);
	}
	
	
	public Type random(){
		
		int size = array.size();
		int index = rand.nextInt(array.size());
		
		if (index < size && index >= 0) {
			return array.get(index);
		}else{
			System.out.println("Problem: " + size + " " + index);
		}
		
		return null;
	}
	
	public void destroy(){
		array.clear();
	}
}
