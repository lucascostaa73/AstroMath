package game;

import java.util.ArrayList;

import org.newdawn.slick.*;
//import org.newdawn.slick.BasicGame;
//import org.newdawn.slick.GameContainer;
//import org.newdawn.slick.Graphics;
//import org.newdawn.slick.SlickException;
//import org.newdawn.slick.Image;
//import org.newdawn.slick.KeyListener;
//import org.newdawn.slick.MouseListener;

import entity.ScoreRank;
import scenes.*;

public class GameController extends BasicGame{
	
	public static GameController singleton;
	
	public int window_width = 1680;//1280;
	public int window_heigth = 1050;//720;
	
	public ArrayList<ScoreRank> scores = new ArrayList<ScoreRank>();
	
	private Scene currentScene = null;
	
	//Sounds
	private Sound wrong;
	private Sound correct;
	private Music greatMusic;
	
	public ExitDelegate exitDelegate = null;
	
	
	public GameController(String gameTitle){
		super(gameTitle);
		GameController.singleton = this;
		
		try {
			greatMusic = new Music("audio/gameMusic.ogg");
			wrong = new Sound("audio/wrong.wav");
			correct = new Sound("audio/correct.wav");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		greatMusic.setVolume(0.7f);
		greatMusic.loop();
	}
	
	public boolean update(){
		
		if (currentScene != null){
			currentScene.update();
		}
		
		return false;
	}
	
	public void transitTo(Scene newScene) {
		
		if (newScene == null){
			System.out.println("null scene");
			return;
		}
		
		if (currentScene != null){
			currentScene.destroy();
		}
		
		currentScene = newScene;
		
		try {
			currentScene.initializeScene(this);
		} catch (SlickException expt) {
			System.out.println(expt);
		}
		
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		Scene scene = new GameScene();
		this.transitTo(scene);
	}
	
	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		if (currentScene != null){
			currentScene.render(g);
		}
		
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		if (currentScene != null){
			currentScene.update();
		}
	}
	
	private int secret = 0; 
	
	public void mousePressed(int button, int x, int y) {
		
	    if( button == 0 ) {
	    	if (currentScene != null){
	    		MouseListener listener = currentScene.getMouseListener();
	    		if (listener != null){
	    			listener.mouseClicked(button, x, y, 0);
	    			
	    			if (x < 90 && y < 90){
	    				secret += 1;
	    			}
	    			
	    		}
	    	}
	    }else{
	    	secret = 0;
	    }
	    
	    if(secret >= 3){
	    	this.currentScene.secret();
	    	secret = 0;
	    }
	    
	}
	
	public void keyPressed(int key, char c){
		if (currentScene != null){
    		KeyListener listener = currentScene.getKeyListener();
    		if (listener != null){
    			listener.keyPressed(key, c);
    		}
    	}
	}
	
	public void keyReleased(int key, char c){
		if (currentScene != null){
			KeyListener listener = currentScene.getKeyListener();
    		if (listener != null){
    			listener.keyReleased(key, c);
    		}
    	}
	}
	
	public void reset(){
		Scene scene = new GameScene();
		this.transitTo(scene);
	}
	
	public void playWrong(){
		this.wrong.play();
	}
	
	public void playCorrect(){
		this.correct.play();
	}
	
	public void exit(){
		if(exitDelegate != null){
			exitDelegate.exit();
		}
	}
	
}
