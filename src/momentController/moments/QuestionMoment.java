package momentController.moments;

import java.util.Timer;
import java.util.TimerTask;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import momentController.GameMomentController;
import momentController.moments.QuestionMomentElements.*;
import scenes.Button;
import scenes.ButtonDelegate;
import scenes.Entity;
import util.*;
import util.delay.Delay;
import util.delay.DelayDelegate;
import entity.*;
import entity.timeBar.*;
import game.GameController;

public class QuestionMoment implements MomentInterface, HeaderCallback, ButtonDelegate, EndTimerDelegate,
		AnimationDelegate, TimeBarDelegate, DelayDelegate {

	private GameMomentController controller;
	private Button touchedButton = null;
	private boolean shouldChangeMoment = false;
	public Animation cloudsAnimation = null;
	private boolean didLeave = false;
	private boolean correctAnswer = false;
	
	
	@Override
	public void begin(GameMomentController momentController) {
		controller = momentController;
		controller.setMoment(this);

		this.cloudsAnimation = controller.cloudsAnimation.copy();
		startGame();
		
		controller.gameLevel += 1.1f;
	
		controller.setGameOverButtonsVibility(false);
		controller.setAnswersVisibility(false);
	}

	@Override
	public void leave(GameMomentController momentController) {
		this.destroyState();
	}

	@Override
	public void update() {
		if (shouldChangeMoment && this.controller != null){
			
			MomentInterface state = new SurvivalMoment();
			state.begin(this.controller);
			destroyState();
		}
	}

	// The writing has ended and The questions appears
	public void writingHasEnded() {

		if (controller == null) { return; }
		
		if (didLeave) { return; }
		
		// Remove answers
		controller.hud.removeAllObjs();

		if (controller.currentQuestion == 0) {
			controller.currentQuestion += 1;
			
			float px = GameController.singleton.window_width*0.40f;
			float py = GameController.singleton.window_heigth*0.40f;
			
			AnswerButton start = createButton(px, py);
			start.scale = 1f;
			start.answer = "START";
			start.state = 0;
			
		} else {

			 controller.bar.maxCounter = 1000;
			 controller.bar.start();
			 controller.bar.delegate = this;

			QuestionAnswerPack q = controller.questions.current();

			int i = 0;
			for (Answer a : q.answers) {
				double angle = (i * Math.PI / (q.answers.size() - 1));
				double cos = GameController.singleton.window_width/5 * Math.cos(angle);
				double sin = i == 0 || i == 3 ? 300 : 100;

				// System.out.println(angle + ": " + cos + " " + sin);
				AnswerButton cloud = createButton(cos + controller.windowWidth/3 + 40, sin + controller.windowHeight/4);
				cloud.answer = " " + a.value;
				cloud.state = 1;
				cloud.type = a.type;

				i += 1;
			}
		}
	}

	private AnswerButton createButton(double x, double y) {
		AnswerButton bt = new AnswerButton(y);
		bt.x = (float) x;
		bt.y = (float) y;
		bt.width = GameController.singleton.window_width/5;
		bt.height = bt.width*0.55f;
		bt.setImage("clouds/cloud_1.png");		

		if (!didLeave) {
			bt.delegate = this;
			controller.hud.addObj(bt);
			controller.answers.add(bt);	
		}

		return bt;
	}

	@Override
	public void didTouched(Button bt) {
		
		if (controller == null) { return; }

		if (bt.getClass() == AnswerButton.class) {

			AnswerButton button = (AnswerButton) bt;

			switch (bt.state) {

			case 0:
				this.startGame();
				bt.enabled = false;
				this.explodeCloud(bt);
				break;
			case 1:
				this.answerTouch(button);
				break;
			default:
				break;
			}
		}
	}

	public void startGame() {
		
		if (controller != null){
			
			if (controller.currentQuestion == 0) {
				// Initial phrase
				controller.header.setText("Tente acertar as respostas das situações apresentadas para ganhar pontos. Quanto maior for sua pontuação, melhor será sua classificação!");
				controller.header.startWriting(this);
			} else {
			
				controller.answers.clear();
				controller.header.setText(" ");
				
				System.out.println("Calling question with dificult: " + controller.gameLevel);
				QuestionAnswerPack q = controller.questions.next((int) controller.gameLevel);

				controller.header.setText(q.question);
				controller.header.startWriting(this);
			}

			touchedButton = null;
		}
	}

	public void answerTouch(AnswerButton button) {

		if (controller == null) { return; }
		
		// System.out.println(button.answer);

		this.touchedButton = button;

		if (button.type == 1) {
			correctAnswer(button);
		} else {
			wrongAnswer(button);
		}

		button.enabled = false;
		button.type = 100;

		this.removeButtonsExcept(button);

		timerDelay();
	}

	//Answer handle
	private void correctAnswer(AnswerButton button){
		if (controller != null){
			
			correctAnswer = true;
			
			controller.bar.finish();
			
			button.color = Color.green;
			controller.life.gainLife();

			controller.coin.quantity += 100;
			GameController.singleton.playCorrect();
			
			Delay delay = new Delay(this, 1);
			delay.schedule(1500);
		}
	}
	
	private void wrongAnswer(AnswerButton button){
		
		if (controller != null){
			
			controller.bar.finish();
			
			button.color = Color.red;
			controller.life.loseLife();
			
			this.checkLife();
			GameController.singleton.playWrong();
		}
	}
	
	//Launch TimerDelay
	private void timerDelay(){
		
		if (controller == null) { return; }
		
		Timer timer = new Timer();
		AnswerLeaving task = new AnswerLeaving();
		task.delegate = this;
		timer.schedule(task, 700);
	}
	
	// Timer from Cloud delay did ended
	public void timerEnded(TimerTask task) {
		
		if (controller == null) { return; }
		
		//Run cloud explosion animation
		explodeCloud(this.touchedButton);
		
		if (correctAnswer == false){ // repeat only if was wrong
			this.startGame();
		}
	}

	// Answer leaving delay
	class AnswerLeaving extends TimerTask {
		public EndTimerDelegate delegate;

		public void run() {
			delegate.timerEnded(this);
		};
	}

	public void removeButtonsExcept(Button otherBt) {

		if (controller != null){
		
			for (AnswerButton bt : controller.answers) {
				if (bt != otherBt) {
					explodeCloud(bt);
				}
			}
		}
	}

	// Cloud explosion
	private void explodeCloud(Button cloud) {
		
		if (cloud != null){
			// System.out.println("Exploding cloud...");
			cloud.enabled = false;
			
			Animation explode = this.cloudsAnimation.copy();
			explode.delegate = this;
			explode.run(cloud);
			
			if (cloud.getClass() == AnswerButton.class){
				((AnswerButton)cloud).answer = "";
			}
		}
	}
	
	private void explodeAllClouds(){
		if (controller != null){
			for (AnswerButton bt : controller.answers) {
				explodeCloud(bt);
			}
		}
	}

	// Animation from entity did Ended
	public void animationDidEnd(Entity entity, Animation animation) {
		
		// System.out.println("Animation ended...");

		if (entity.getClass() == AnswerButton.class) {
			AnswerButton button = (AnswerButton) entity;

			button.color = Color.transparent;
			button.enabled = false;
			button.visible = false;
			button.answer = "";
		}
	}

	@Override
	public void render(Graphics g) {
		if (controller == null){ return; }
		
		if (controller.header != null) {
			controller.header.render(g);
		}
	}

	@Override
	public void timerDidEnd() {
		if (controller == null){ return; }
		
		if (touchedButton == null){
			controller.life.loseLife();
			checkLife();
		}
		
		explodeAllClouds();
		
		this.startGame();
	}
	
	private void destroyState(){
		didLeave = true;
		this.explodeAllClouds();
		controller.answers.clear();
		this.controller = null;
	}
	
	private void checkLife(){
		if (controller.life.getLife() <= 0){
			GameOverMoment m = new GameOverMoment();
			m.begin(controller);
			destroyState();
		}
	}

	@Override
	public void delayEnded(Delay delay) {
		if (delay.getType() == 1){
			this.shouldChangeMoment = true;
		}
		
	}

}
