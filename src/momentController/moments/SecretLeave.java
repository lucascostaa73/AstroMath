package momentController.moments;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;

import entity.timeBar.TimeBarDelegate;
import game.GameController;
import momentController.GameMomentController;
import scenes.Button;
import scenes.ButtonDelegate;
import util.delay.Delay;
import util.delay.DelayDelegate;


public class SecretLeave implements MomentInterface, DelayDelegate, TimeBarDelegate, ButtonDelegate {

	
	private ArrayList<String> commands = new ArrayList<String>();
	
	@Override
	public void didTouched(Button bt) {
		// TODO Auto-generated method stub
		
		commands.add(String.valueOf(bt.type));
		
		if(commands.size() >= 4){
			
			if (commands.get(0).equals("1")
					&& commands.get(1).equals("1") 
					&& commands.get(2).equals("2")  
					&& commands.get(3).equals("4")){
				
				GameController.singleton.exit();
			}else {
				GameController.singleton.reset();
			}
			
		}
	}

	@Override
	public void timerDidEnd() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delayEnded(Delay delay) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void begin(GameMomentController momentController) {
		momentController.setMoment(this);
		
		momentController.bar.delegate = this;
		
		momentController.up.delegate = this;
		momentController.up.type = 1;
		
		momentController.down.delegate = this;
		momentController.down.type = 2;
		
		momentController.left.delegate = this;
		momentController.left.type = 3;
		
		momentController.right.delegate = this;
		momentController.right.type = 4;
		
		//Visibility
		momentController.addButtonsToHud();
		momentController.setGameOverButtonsVibility(true);
		momentController.setBarVisibility(false);
		momentController.setAnswersVisibility(false);
		
	}

	@Override
	public void leave(GameMomentController momentController) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		
	}

}
