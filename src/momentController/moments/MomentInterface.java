package momentController.moments;

import org.newdawn.slick.Graphics;

import momentController.GameMomentController;

public interface MomentInterface {
	public void begin(GameMomentController momentController);
	public void leave(GameMomentController momentController);
	
	public void update();
	public void render(Graphics g);
}
