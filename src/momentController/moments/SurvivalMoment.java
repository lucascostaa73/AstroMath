package momentController.moments;

import java.util.ArrayList;
import entity.AsteroidMath;
import scenes.Button;
import scenes.ButtonDelegate;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import entity.life.LifeEndedDelegate;
import entity.timeBar.TimeBarDelegate;
import game.GameController;
import momentController.GameMomentController;
import scenes.Entity;
import util.Array;

public class SurvivalMoment implements MomentInterface, TimeBarDelegate, LifeEndedDelegate, ButtonDelegate{

	private ArrayList<Entity> entities;
	private ArrayList<Entity> dead;
	private GameMomentController controller;
	private Array<MathCount> counts = new Array<MathCount>();
	private Array<Double> sizes = new Array<Double>(1.0, 0.9, 0.85);
	
	private int inBettwenIterator = 0;
	
	public SurvivalMoment(){
		
		System.out.println("Survival MODE !");
		
		entities = new ArrayList<Entity>();
		dead = new ArrayList<Entity>();
	}

	@Override
	public void begin(GameMomentController momentController) {
		controller = momentController;
		controller.setMoment(this);
		
		controller.bar.delegate = this;
		controller.bar.maxCounter = 2000;
		controller.bar.start();
		
		
		if (this.controller.gameLevel < 3) {
			this.addAditionSubtraction();
		}
		
		if (this.controller.gameLevel > 2){
			this.addMultiply();
		}
		
		if (this.controller.gameLevel > 4){
			this.addDivision();
		}
		
		controller.setAnswersVisibility(false);
	}

	@Override
	public void leave(GameMomentController momentController) {
		
	}

	@Override
	public void update() {
		
		for(int i = 0; i < entities.size(); i++){
			AsteroidMath a = (AsteroidMath) entities.get(i);
			
			//System.out.println("asteroidy: " + a.y + " " + GameController.singleton.window_heigth + " " + a.height);
			
			if (a.y > GameController.singleton.window_heigth - a.height ){
				if (a.type == 0){
					correctAnswer(a);
				}else{
					this.wrongAnswer(a);
				}
			}
			
			if (a.y > GameController.singleton.window_heigth + a.height) {
				dead.add(a);
			}
		
		}
		
		for(Entity ent: dead){
			entities.remove(ent);
			controller.hud.removeObj((Button) ent);
		}
		
		dead.clear();
		
		if (inBettwenIterator <= 0){
			this.createAsteroid(1, sizes.random().floatValue());
			if (controller.gameLevel > 0) {
				inBettwenIterator = (int) (Math.random()*200/(0.5*controller.gameLevel) + (100 - 10*controller.gameLevel));
			}else{
				inBettwenIterator = 200;
			}
		}
		
		this.inBettwenIterator -= 1;
	}

	@Override
	public void render(Graphics g) {
		
	}
	
	private Entity asteroid(){
		AsteroidMath asteroid = new AsteroidMath();
		
		MathCount c = this.counts.random();
		asteroid.type = c.correct;
		asteroid.math = c.value;
		asteroid.delegate = this;
		
		asteroid.setImage(controller.asteroidImages.random() );
		
		controller.hud.addObj(asteroid);
		entities.add(asteroid);
		
		return asteroid;
	}
	
	private Entity createAsteroid(int position, float scale){
		
		Entity a = asteroid();
		
		a.width = GameController.singleton.window_width/8;
		a.height = a.width;
		a.velocityY = (float) Math.random()*(2) + 3;
		a.scale = (float) (Math.random()*0.7 + 0.7);
		
		a.x = (float) (Math.random()*(controller.windowWidth - a.width*a.scale*2) + a.width*a.scale) ;
		a.y = -(a.height*a.scale);
		
		System.out.println("Creating asteroid");
		
		return a;
	}

	@Override
	public void lifeDidEnded() {
		// TODO Auto-generated method stub
	}

	@Override
	public void timerDidEnd() {
		QuestionMoment state = new QuestionMoment();
		state.begin(controller);
	}

	@Override
	public void didTouched(Button bt) {
		if(bt.type == 1){
			correctAnswer((AsteroidMath) bt);
		}else{
			wrongAnswer((AsteroidMath) bt);
		}
		
		updateComboImage();
	}
	
	public void correctAnswer(AsteroidMath bt){
		
		if (bt.clicked == false){
			bt.clicked = true;
			controller.combo += 1;

			controller.coin.quantity += 20*controller.getMultiplier();
			bt.color = Color.green;
			GameController.singleton.playCorrect();
		}
	}
	
	public void wrongAnswer(AsteroidMath bt){
		
		if (bt.clicked == false){
			bt.clicked = true;
			controller.combo = 0;
			controller.life.loseLife();
			bt.color = Color.red;
			GameController.singleton.playWrong();

			if (controller.life.getLife() <= 0){
				GameOverMoment m = new GameOverMoment();
				m.begin(controller);
			}
		}
	}
	
	public void updateComboImage(){
		
	}
	
	public class MathCount {
		public String value = "";
		public int correct = 1;
		
		public MathCount(String v,int c){
			this.value = v;
			this.correct = c;
		}
	}
	
	private void addMultiply(){
		counts.add(new MathCount("2x2 = 4", 1));
		counts.add(new MathCount("2x3 = 6", 1));
		counts.add(new MathCount("5x3 = 15", 1));
		counts.add(new MathCount("3x5 = 9", 0));
		counts.add(new MathCount("9x9 = 81", 1));
		counts.add(new MathCount("4x7 = 28", 1));
		counts.add(new MathCount("5x8 = 40", 1));
		counts.add(new MathCount("3x3 = 9", 1));
		counts.add(new MathCount("2x9 = 18", 1));
		counts.add(new MathCount("10x2 = 20", 1));
		counts.add(new MathCount("9x4 = 26", 0));
		counts.add(new MathCount("9x4 = 36", 1));
		counts.add(new MathCount("1x9 = 90", 0));
		counts.add(new MathCount("1x9 = 9", 1));
		counts.add(new MathCount("7x3 = 23", 0));
		counts.add(new MathCount("7x3 = 21", 1));
		counts.add(new MathCount("7x7 = 49", 1));
		counts.add(new MathCount("4x4 = 16", 1));
		counts.add(new MathCount("9x5 = 35", 0));
		counts.add(new MathCount("9x5 = 45", 1));
		counts.add(new MathCount("4x8 = 60", 0));
		counts.add(new MathCount("7x5 = 23", 0));
		counts.add(new MathCount("7x5 = 35", 1));
		counts.add(new MathCount("7x9 = 49", 0));
		counts.add(new MathCount("7x9 = 63", 1));
		counts.add(new MathCount("4x8 = 16", 0));
		counts.add(new MathCount("4x6 = 16", 0));
		counts.add(new MathCount("9x3 = 27", 1));
		counts.add(new MathCount("8x8 = 64", 1));
		counts.add(new MathCount("4x3 = 12", 1));
		counts.add(new MathCount("4x3 = 16", 0));
		counts.add(new MathCount("9x3 = 27", 1));
		counts.add(new MathCount("8x4 = 32", 1));
	}
	
	private void addDivision(){
		counts.add(new MathCount("2÷2 = 1", 1));
		counts.add(new MathCount("4÷2 = 2", 1));
		counts.add(new MathCount("16÷8 = 2", 1));
		counts.add(new MathCount("21÷3 = 7", 1));
		counts.add(new MathCount("6÷3 = 2", 1));
		counts.add(new MathCount("18÷9 = 2", 1));
		counts.add(new MathCount("40÷4 = 10", 1));
		counts.add(new MathCount("16÷4 = 4", 1));
		counts.add(new MathCount("49÷7 = 7", 1));
		counts.add(new MathCount("55÷5 = 11", 1));
		counts.add(new MathCount("25÷5 = 5", 1));
		
		counts.add(new MathCount("2÷2 = 2", 0));
		counts.add(new MathCount("4÷2 = 8", 0));
		counts.add(new MathCount("16÷8 = 4", 0));
		counts.add(new MathCount("21÷3 = 9", 0));
		counts.add(new MathCount("6÷3 = 3", 0));
		counts.add(new MathCount("18÷9 = 11", 0));
		counts.add(new MathCount("40÷4 = 12", 0));
		counts.add(new MathCount("16÷4 = 8", 0));
		counts.add(new MathCount("49÷7 = 9", 0));
		counts.add(new MathCount("55÷5 = 5", 0));
		counts.add(new MathCount("25÷5 = 7", 0));
		
	}
	
	private void addAditionSubtraction(){
		counts.add(new MathCount("2+2 = 4", 1));
		counts.add(new MathCount("2+3 = 5", 1));
		counts.add(new MathCount("5+3 = 8", 1));
		counts.add(new MathCount("13+4 = 17", 1));
		counts.add(new MathCount("26+10 = 36", 1));
		counts.add(new MathCount("14+3 = 17", 1));
		counts.add(new MathCount("9+7 = 16", 1));
		
		counts.add(new MathCount("9+9 = 29", 0));
		counts.add(new MathCount("17+2 = 21", 0));
		counts.add(new MathCount("5+8 = 15", 0));
		counts.add(new MathCount("3+7 = 12", 0));
		counts.add(new MathCount("2+5 = 6", 0));
		counts.add(new MathCount("4+2 = 8", 0));
		
		counts.add(new MathCount("2-2 = 0", 1));
		counts.add(new MathCount("3-2 = 1", 1));
		counts.add(new MathCount("5-3 = 2", 1));
		counts.add(new MathCount("13-4 = 9", 1));
		counts.add(new MathCount("26-10 = 16", 1));
		counts.add(new MathCount("14-3 = 11", 1));
		
		counts.add(new MathCount("9-7 = 3", 0));
		counts.add(new MathCount("9-6 = 1", 0));
		counts.add(new MathCount("17-2 = 20", 0));
		counts.add(new MathCount("10-5 = 7", 0));
		counts.add(new MathCount("30-7 = 21", 0));
		counts.add(new MathCount("15-5 = 20", 0));
		counts.add(new MathCount("4-2 = 3", 0));
	}
}
