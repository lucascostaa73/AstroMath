package momentController.moments.QuestionMomentElements;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import scenes.Button;
import util.AnimationMoviment;


public class AnswerButton extends Button {
	public boolean correct = false;
	public String answer = " ";
	private AnimationMoviment easyInOut = new AnimationMoviment();
	private double initialPos = 0;

	public AnswerButton(double initialPos){
		super();
		this.initialPos = initialPos;
		easyInOut.iteration = Math.random()*120;
	}

	@Override
	public void draw(Graphics g){
		super.draw(g);
		
		float scale = this.scale > 1f? 1 + ((this.scale - 1)/2): 1;
		
		g.scale(scale, scale);
		g.setColor(Color.black);
		g.drawString(answer, this.centerX() - answer.length()*5 - 10, this.centerY());
		g.scale(1/scale, 1/scale);
	}

	public void update(){
		super.update();
		double mov = easyInOut.easyInOutRepeatUpdate(2, 8);
		this.y = (float) ( mov + initialPos);
	}
}

