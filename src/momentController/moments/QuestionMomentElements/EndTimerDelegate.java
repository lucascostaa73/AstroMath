package momentController.moments.QuestionMomentElements;

import java.util.TimerTask;

public interface EndTimerDelegate {
	public void timerEnded(TimerTask task);
}
