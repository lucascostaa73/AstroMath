package momentController.moments.QuestionMomentElements;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.util.Timer;
import java.util.TimerTask;


public class QuestionHeader {

	private Image image;
	private String text;
	private String writing;
	private Timer writingTimer;
	private WritingLetters writingTask;
	private HeaderCallback obj = null;
	private int currentLetterIndex = 0;
	
	public float x = 0;
	public float y = 0;
	public float w = 0;
	public float h = 0;
	
	public Boolean visible = true; 
	
	class WritingLetters extends TimerTask {
		
		public QuestionHeader header = null;
		
        public void run() {
        	
        	currentLetterIndex += 1;
        	
        	if (currentLetterIndex >= text.length()){
        		writingTimer.cancel(); //Terminate the timer thread
        		if (obj != null){
        			obj.writingHasEnded();
        		}
        		writingTimer = null;
        		writingTask = null;
        		currentLetterIndex = 0;
        		return;
        	}
        	
        	writing = text.substring(0, currentLetterIndex + 1);
        	
        	char current_char = text.charAt(currentLetterIndex);
        	int delay = 60;
        	
        	if (currentLetterIndex % (((int) header.w/20)) == 0){
        		
        		if (text.charAt(currentLetterIndex) == ' '){
        			text = text.substring(0, currentLetterIndex + 1) 
							+ "\n" 
							+ text.substring(currentLetterIndex + 1, text.length());
        			
        		}else if(text.charAt(currentLetterIndex + 1) == ' ' ){
        			text = text.substring(0, currentLetterIndex + 2) 
							+ "\n" 
							+ text.substring(currentLetterIndex + 2, text.length());
        			
        		}else if(text.charAt(currentLetterIndex - 1) == ' '){
        			text = text.substring(0, currentLetterIndex) 
							+ "\n" 
							+ text.substring(currentLetterIndex, text.length());
        		}else{
        			text = text.substring(0, currentLetterIndex) 
							+ "-\n" 
							+ text.substring(currentLetterIndex, text.length());
        		}
        		
        		currentLetterIndex += 1;
        		
        	}
        	

        	if (current_char == ' '){
        		delay -= 40;
        	}
        	else if (current_char == ','){
        		delay += 200;
        	}
        	else if (current_char == '.' || current_char == '!' || current_char == '?'){
        		delay += 600;
        	}
        	
        	// trigger next letter
        	writingTask = new WritingLetters();
        	writingTask.header = this.header;
        	writingTimer.schedule(writingTask, delay);
        }
    }
	
	public void setImage(String assetName){
		try {
			this.image = new Image("assets/" + assetName);
		} catch (SlickException e) {
			System.out.println("Could not load image");
		}
	}
	
	public void update(){
		
	}
	
	public void render(Graphics g){
		
		if (!visible) { return; }
		
		image.draw(x,y,w,h);
		float scale = 2f;
		g.scale(scale, scale);
		g.setColor(Color.black);
		g.drawString(writing, 100/scale, 80/scale);
		g.scale(1/scale, 1/scale);
	}
	
	public void setText(String text){
		this.text = text + " ";
		this.writing = " ";
	}
	
	public void startWriting(HeaderCallback someObj){
		
		System.out.println("Start writing...");
		writingTimer = new Timer();
		writingTask = new WritingLetters();
		writingTask.header = this;
		
		this.obj = someObj;
		
		writingTimer.schedule(writingTask, 100);
	}
	
	public void forcefinishWriting(){
		writingTask.cancel();
		writing = text;
	}
	
	
}
