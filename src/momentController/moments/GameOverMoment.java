package momentController.moments;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import game.GameController;
import entity.ScoreRank;
import entity.timeBar.TimeBarDelegate;
import momentController.GameMomentController;
import scenes.Button;
import scenes.ButtonDelegate;
import util.delay.*;

public class GameOverMoment implements MomentInterface, DelayDelegate, TimeBarDelegate, ButtonDelegate {

	
	private GameMomentController controller;
	private ScoreRank currentRank = null;
	
	private int currentLetterIndex = 0;
	private char currentLetter = 'A';
	
	
	@Override
	public void begin(GameMomentController momentController) {
		System.out.println("Game OVER !");
		controller = momentController;
		momentController.setMoment(this);
		
		momentController.bar.delegate = this;
		
		momentController.up.delegate = this;
		momentController.up.type = 1;
		
		momentController.down.delegate = this;
		momentController.down.type = 2;
		
		momentController.left.delegate = this;
		momentController.left.type = 3;
		
		momentController.right.delegate = this;
		momentController.right.type = 4;
		
		//Visibility
		momentController.addButtonsToHud();
		momentController.setGameOverButtonsVibility(true);
		momentController.setBarVisibility(false);
		momentController.setAnswersVisibility(false);
		
		
		ScoreRank r = controller.board.checkBestScore(momentController.coin.quantity);
		if (r != null){
			r.name = "AAA";
			currentRank = r;
		}
		
		//(new Delay(this, 1)).schedule(10000);
	}

	@Override
	public void leave(GameMomentController momentController) {
		
	}

	@Override
	public void update() {
		
	}

	@Override
	public void render(Graphics g) {
		if(controller != null){
			controller.board.draw(g);
		}
	}

	@Override
	public void delayEnded(Delay delay) {
		
		//System.out.println("Delay ended !");
		
		controller.board.clear();
		
		QuestionMoment m = new QuestionMoment();
		m.begin(controller);
	}

	@Override
	public void timerDidEnd() {
		
	}

	@Override
	public void didTouched(Button bt) {
		
		if (currentRank == null) {return;}
		
		
		//System.out.println(bt.type);
		//System.out.println(currentRank.name + " " + currentLetterIndex + " " + currentLetter + ":" + (int)currentLetter);
		
		switch(bt.type){
		case 1:
			currentLetter += 1;
			correctLetter();
			currentRank.name = changeChar(currentRank.name, currentLetterIndex, currentLetter);
			break;
		case 2:
			currentLetter -= 1;
			correctLetter();
			currentRank.name = changeChar(currentRank.name, currentLetterIndex, currentLetter);
			break;
		case 3:
			System.out.println("LEFT BUTTON");
			currentLetterIndex -= 1;
			currentLetter = 65;
			correctLetterIndex();
			break;
		case 4:
			
			if(currentLetterIndex == 2){
				GameController.singleton.reset();
			}
			
			currentLetterIndex += 1;
			currentLetter = 65;
			correctLetterIndex();
			
			break;
		default:
			break;
		}
		
		if(currentLetterIndex == 2){
			controller.right.color = Color.red;
		}else{
			controller.right.color = Color.white;
		}
		
		
	}
	
	private String changeChar(String str, int i, char c){
		return str.substring(0,i) + c + str.substring(i+1);
	}
	
	private void correctLetter(){
		if (currentLetter < 65){
			currentLetter = 90;
		}
		if (currentLetter > 90){
			currentLetter = 65;
		}
	}
	
	private void correctLetterIndex(){
		if (currentLetterIndex < 0){
			currentLetterIndex = 0;
		}
		if (currentLetterIndex > 2){
			currentLetterIndex = 2;
		}
	}

}
