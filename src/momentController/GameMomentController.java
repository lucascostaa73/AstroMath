package momentController;

import java.util.ArrayList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import entity.timeBar.*;
import game.GameController;
import momentController.moments.*;
import momentController.moments.QuestionMomentElements.AnswerButton;
import momentController.moments.QuestionMomentElements.QuestionHeader;
import scenes.Button;
import scenes.ButtonDelegate;
import scenes.HudManager;
import util.Array;
import util.Questions;
import entity.Animation;
import entity.Coin;
import entity.RankingBoard;
import entity.life.*;

public class GameMomentController {

	public int windowWidth;
	public int windowHeight;
	public HudManager hud;
	public TimeBar bar = new TimeBar();
	public PlayerLife life = new PlayerLife();
	public Coin coin = new Coin();
	public RankingBoard board = new RankingBoard();
	public int combo = 0;
	public float gameLevel = 0;
	
	//Question State
	public QuestionHeader header;
	public ArrayList<AnswerButton> answers = new ArrayList<AnswerButton>();
	public int currentQuestion = 0;
	public Questions questions = new Questions();
	
	//Animation
	public Animation cloudsAnimation = new Animation();
	
	//Math State
	public Array<Image> asteroidImages = new Array<Image>();
	
	public int difficult;
	private MomentInterface moment;
	
	//Buttons GameOver
	public Button up = new Button();
	public Button down = new Button();
	public Button left = new Button();
	public Button right = new Button();
	
	
	
	public void start(){
		
		//arrow
		up.setImage("arrow_up.png");
		down.setImage("arrow_down.png");
		left.setImage("arrow_left.png");
		right.setImage("arrow_right.png");
		
		hud.addObj(up);
		hud.addObj(down);
		hud.addObj(left);
		hud.addObj(right);
		
		float size = 100;
		
		up.width = size;
		up.height = size;
		up.x = this.windowWidth/2 - up.width/2;
		up.y = this.windowHeight - up.height*4;
		
		down.width = size;
		down.height = size;
		down.x = this.windowWidth/2 - down.width/2;
		down.y = this.windowHeight - down.height*2;
		
		left.width = size;
		left.height = size;
		left.x = this.windowWidth/2 - left.width*2;
		left.y = this.windowHeight - left.height*3;
		
		right.width = size;
		right.height = size;
		right.x = this.windowWidth/2 + right.width;
		right.y = this.windowHeight - right.height*3;
		

		
		
		board.setImage("board.png");
		board.width = this.windowWidth/3;
		board.height = this.windowHeight/2;
		
		board.x = this.windowWidth/2 - board.width/2;
		board.y = 100;
		
		cloudsAnimation.frameDelay = 3;
		cloudsAnimation.setImages("assets/clouds/cloud_", 6);
		
		MomentInterface moment = new QuestionMoment();
		
		loadImages();
		
		configureLife();
		configureTimeBar();
		configureCoins();
		createHeader();
		
		moment.begin(this);
	}
	
	private void configureLife(){
		//Configure lifes
		life.width = GameController.singleton.window_width/7;
		life.height = life.width/3;
		
		life.x = this.windowWidth - 30*life.getMax();
		life.y = this.windowHeight - life.height - 10;
		
		life.inBetween = 10;
	}
	
	private void configureTimeBar(){
		//Configure bar
		
		bar.height = 20;
		bar.width = windowWidth/2;

		bar.x = (windowWidth/2f) - bar.width/2;
		bar.y = 0;
	}
	
	private void configureCoins(){
		
		coin.width = GameController.singleton.window_width/12;
		coin.height = coin.width;
		
		coin.x = 10;
		coin.y = this.windowHeight - coin.height - 20;
	}
	
	public void loadImages(){
		
		try {
			asteroidImages.add(new Image("assets/asteroid_1.png") );
			asteroidImages.add(new Image("assets/asteroid_2.png") );
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	//State interface
	public void setMoment(MomentInterface newMoment){
		bar.delegate = null;
		this.setAnswerDelegate(null);
		this.setGameOverButtonsDelegate(null);
		bar.finish();
		moment = newMoment;
	}
	
	public void begin(){
		if (moment != null){
			moment.begin(this);
		}
	}
	
	public void leave(){
		if (moment != null){
			moment.leave(this);
		}
	}
	
	public void update(){
		if (moment != null){
			moment.update();
			bar.update();
			life.update();
			coin.update();
		}
	}
	
	public void render(Graphics g){
		if (moment != null){
			moment.render(g);
			bar.draw(g);
			life.draw(g);
			coin.draw(g);
		}
	}
	
	private void createHeader() {
		header = new QuestionHeader();
		header.setImage("header.png");
		
		header.w = GameController.singleton.window_width;
		header.h = GameController.singleton.window_heigth;

		currentQuestion = 0;
	}
	
	// MARK Visibility
	public void setBarVisibility(Boolean visible){
		this.bar.visible = visible;
	}
	
	public void setGameOverButtonsVibility(Boolean visible){
		this.up.visible = visible;
		this.down.visible = visible;
		this.left.visible = visible;
		this.right.visible = visible;
	}
	
	public void setHeaderVisibility(Boolean visible){
		this.header.visible = visible;
	}
	
	public void setAnswersVisibility(Boolean visible){
		for (int i = 0; i < this.answers.size(); i++){
			AnswerButton a = this.answers.get(i);
			a.visible = visible;
		}
	}
	
	
	// Mark Delegate
	public void setAnswerDelegate(ButtonDelegate delegate){
		for (int i = 0; i < this.answers.size(); i++){
			AnswerButton a = this.answers.get(i);
			a.delegate = delegate;
		}
	}
	
	public void setGameOverButtonsDelegate(ButtonDelegate delegate){
		this.up.delegate = delegate;
		this.down.delegate = delegate;
		this.left.delegate = delegate;
		this.right.delegate = delegate;
	}
	
	public void addButtonsToHud(){
		this.hud.addObj(up);
		this.hud.addObj(down);
		this.hud.addObj(left);
		this.hud.addObj(right);
	}
	
	public double getMultiplier(){
		
		double m = 1;
		
		if (combo < 4) {
			m = 1;
		}else if (combo < 10) {
			m = 2;
		}else if (combo < 20) {
			m = 3;
		}else if (combo < 30) {
			m = 4;
		}else if (combo < 50) {
			m = 5;
		}else {
			m = 10;
		}
		
		return m;
	}
	
	public void secret(){
		
		this.moment.leave(this);
		
		MomentInterface moment = new SecretLeave();		
		moment.begin(this);
	}
}
